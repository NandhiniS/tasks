package com.parsing.core;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nandhini
 *
 */
public class BookBeans {
	private String bookType;
	private String bookTitle;
	private ArrayList<String> authorList = new ArrayList<String>();

	public void setAuthorList(List<String> authorList) {
		this.authorList = (ArrayList<String>) authorList;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public ArrayList<String> getAuthorList() {
		return authorList;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookISBN() {
		return bookISBN;
	}

	public void setBookISBN(String bookISBN) {
		this.bookISBN = bookISBN;
	}

	private String bookISBN;

	@Override
	public String toString() {
		return "\n\t BookBeans \n\n bookType=" + bookType + "\n authorList="
				+ authorList + "\n bookTitle=" + bookTitle + " \n bookISBN="
				+ bookISBN+"\n" ;
	}

	public void addAuthor(String str) {
		this.authorList.add(str);
	}

}
