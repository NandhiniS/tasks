package com.parsing.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author nandhini
 * 
 */
public class ParseImplementation {
	
	ArrayList<BookBeans> bookDetail = new ArrayList<BookBeans>();
	BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
	BookBeans bookObj;
	Document doc;
	int flag = 0;
	
	/**
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public ParseImplementation() throws ParserConfigurationException, SAXException, IOException {
		
		readXML();
		parseXML();
	}

	
	/** Read XML file contents
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void readXML() throws ParserConfigurationException, SAXException, IOException {
		
		try {
			DocumentBuilderFactory documentFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder documentBuilder = documentFactory
					.newDocumentBuilder();
			documentFactory.setValidating(false);
			documentFactory.setNamespaceAware(true);
			documentBuilder.setErrorHandler(new SimpleErrorHandler());
			doc = documentBuilder.parse("XMLFiles/BookListFile.xml");
		} catch (FileNotFoundException fileExp) {
			System.out.println("FileNotFound-Enter Valid file");
		}
	}
	

	/**  Parsing XML file
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void parseXML() throws ParserConfigurationException, SAXException, IOException {

		NodeList bookList = doc.getElementsByTagName("book");
		for (int i = 0; i < bookList.getLength(); i++) {
			Node node = bookList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element book = (Element) node;
				printXML(book);
			}
		}
		System.out.println("Total Books : " + bookDetail.size());
	}
	

 	/** Set the xml data into BookBeans
	 * @param book
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void printXML(Element book) throws ParserConfigurationException, SAXException, IOException { 
		
		bookObj = new BookBeans();
		if (book.getAttribute("type") != "") {
			bookObj.setBookType(book.getAttribute("type"));
			NodeList authList = book.getElementsByTagName("author");
			for (int x = 0; x < authList.getLength(); x++) {
				bookObj.addAuthor(authList.item(x).getTextContent());
			}
			bookObj.setBookTitle(book.getElementsByTagName("title").item(0).getTextContent());
			bookObj.setBookISBN(book.getElementsByTagName("isbn").item(0).getTextContent());
		}
		bookDetail.add(bookObj);
		getBookDetails(bookObj);
	}
	

	/** Find book detail based on book title @param bookTitle */
	public void searchBooks(String bookTitle) throws ParserConfigurationException, SAXException, IOException {

		int found = 0;
		for (BookBeans book : bookDetail) {
			if (book.getBookTitle().equalsIgnoreCase(bookTitle)) {
				found++;
				getBookDetails(book);
			}
		}
		if (found == 0) {
			System.out.println("No Such book");
		}
	}
	

	/**  Delete book based on type or ISBN	 */
	public void deleteBooks(String bookType) throws IOException, ParserConfigurationException, SAXException {

		int count = 0;
		for (BookBeans bb1 : bookDetail) {
			if (bb1.getBookType().equalsIgnoreCase(bookType)) {
				count++;
				getBookDetails(bb1);
			}
		}
		System.out.println("No.of books with same type:" + count);

		switch (count) {
		/** Check valid Book*/
		case 0:
			
			System.out.println("No such Book! Enter Valid Book Type");
			break;
			
		/** Delete Based on book type */	
		case 1:
			
			System.out.println("Are You Sure you want to delete? Type YES to delete or NO to cancel");
			String option = inputReader.readLine();
			if (option.equalsIgnoreCase("YES")) {
				Iterator<BookBeans> it = bookDetail.iterator();
				while (it.hasNext()) {
					if (it.next().getBookType().equalsIgnoreCase(bookType)) {
						it.remove();
						System.out.println("Book Deleted with Type:" + bookType);
					}
				}

			}
			break;
			
			/** Delete Based on book ISBN */
		default:
			
			System.out.println("one or more books having same type:Enter ISBN to delete");
			String bookISBN = inputReader.readLine();
			Iterator<BookBeans> it = bookDetail.iterator();
			while (it.hasNext()) {
				if (it.next().getBookISBN().equalsIgnoreCase(bookISBN)) {
					System.out.println("Are You Sure you want to delete? Type YES to delete or NO to cancel");
					String option1 = inputReader.readLine();
					if (option1.equalsIgnoreCase("YES")) {
						it.remove();
						System.out.println("Book Deleted with Type:" + bookType + "  and ISBN:" + bookISBN);
					} else {
						System.out.println("Not Deleted");
					}
				}
			}
		}
	}

	
	/**  Insert new Book Detail
	 * @param insertType
	 * @param insertTitle
	 * @param insertauthor
	 * @param insertISBN
		 */
	public void insertBooks(String insertType, String insertTitle, List<String> insertauthor, String insertISBN)
			throws ParserConfigurationException, SAXException, IOException {

		Iterator<BookBeans> it = bookDetail.iterator();
		flag = 0;
		while (it.hasNext()) {
			if (it.next().getBookISBN().equalsIgnoreCase(insertISBN)) {
				flag = 1;
				System.out.println("Already book present with same ISBN");
			}
		}
		if (flag == 0) {
			bookObj = new BookBeans();
			bookObj.setBookType(insertType);
			bookObj.setBookTitle(insertTitle);
			bookObj.setAuthorList(insertauthor);
			bookObj.setBookISBN(insertISBN);
			bookDetail.add(bookObj);
			getBookDetails(bookObj);
			System.out.println("Added Successfully");
		}

	}

	
	/** Write updated details into XML File
	 * @throws IOException
	 */
	public void generateXML() throws IOException {
		
		BookBeans bObj;
		ArrayList<String> auObj;
		File infile = new File("XMLFiles/TempFile.xml");
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(infile));
			out.println("<booklist>");
			for (int i = 0; i < bookDetail.size(); i++) {
				bObj = bookDetail.get(i);
				out.println("<book type=\"" + bObj.getBookType() + "\">");
				auObj = bObj.getAuthorList();
				for (int z = 0; z < auObj.size(); z++) {
					out.println("\t<author>" + auObj.get(z) + "</author>");
				}
				out.println("\t<title>" + bObj.getBookTitle() + "</title>");
				out.println("\t<isbn>" + bObj.getBookISBN() + "</isbn>");
				out.println("\t</book>");
			}
			out.println("</booklist>");
			out.flush();
			infile.renameTo(new File("XMLFiles/BookListFile.xml"));
		} catch (FileNotFoundException exp) {
			System.out.println("Not Found");
		} finally {
			out.close();
		}

	}

	/**	View all the book details
	 * @param bookObj
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void getBookDetails(BookBeans bookObj)throws ParserConfigurationException, SAXException, IOException {

		System.out.println("Type:" + bookObj.getBookType());
		System.out.println("Title:" + bookObj.getBookTitle());
		System.out.println("Authors :" + bookObj.getAuthorList());
		System.out.println("ISBN :" + bookObj.getBookISBN());
		System.out.println("=========================");
		
	}
	
}/** End of class */
