package com.parsing.core;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author nandhini
 *
 */
public class SimpleErrorHandler implements ErrorHandler {
	
	/** @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException) */
	public void warning(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());
	}

	/**  @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException */
	
	public void error(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());
	}

	/** @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException) */
	public void fatalError(SAXParseException e) throws SAXException {
		System.out.println(e.getMessage());

	}
}
