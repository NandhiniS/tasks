import java.lang.*;
import java.util.*;
public class RemDuplicates {
	public static void main(String args[])
	{
		int[] numbers=new int[10];
		Scanner sn=new Scanner(System.in);
		try
		{
		System.out.println("Enter the Numbers:");
		for(int i=0;i<numbers.length;i++)
		{
			numbers[i]=sn.nextInt();
		}
		int result[]=removeDuplicate(numbers);
		System.out.print("The distinct numbers are:\n");
		
			for(int i=0;i<result.length;i++)
			{
				System.out.println(result[i]);
				
			}
		System.out.print("\n Total Distinct numbers:"+result.length);
		}
		catch(InputMismatchException exception)
	       {
	    	   System.out.println("Enter Integer Only");
	       
	      }
	}
	
	public static int[] removeDuplicate(int[] numbers) {
	    int[] temp = new int[numbers.length];
	    int size = 0;
	    for (int i = 0; i < numbers.length; i++) {
	      int Flag=0;
	      for (int j = 0;j<i; j++) {
	        if (numbers[j] == numbers[i]) {
	          Flag = 1;
	        }
	      }
	      if (Flag==0) {
	        temp[size] = numbers[i];
	        size++;
	      }
	    }

	    int[] result = new int[size];
	    for (int i = 0; i < size; i++) {
	      result[i] = temp[i];
	    }
	    return result;
	  }

}
